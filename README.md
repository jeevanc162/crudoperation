# crudoperation
                            **Crud Operation Demo Application**


About Project :-
    This project is used the Python Framework called Django.
        Technology used by the Project :-
            1. Python Programming  
            2. Django Framework version 3.1 
            3. python library fro xlsx file upload  - import_export
            4. mysql Database
            5. Local Server
    Features (functionality) of the Project :-
            1. Upload a xlsx sheet the create Person Infomation.
            2. Create the Person with Informantion.
            3. Delete the Person Informantion from Database.
            4. Update the Person Informantion.
            5. List Out the all Person Information.

    How to make setup for the Project :-
        0. First make sure you have a Python install in your System 
        
        1.  Create a vertual Envernment for the project
                virtualenv -p python3 venv
        2.  Activate the Python Vartual Enverment
                source venv/bin/activate
        3.  Install Django with the following command
                pip install django
        4.  Install the import_export library for uploading the xlsx file
                pip install django-import-export
        
        5.  Clone the repo of git hub 
        6.  Run the Local Server of the project 
                python3 manage.py runserver

        7. Open the URL from the web 
                Localhost:8000
    
    All done !!!

    Have any Queary than Please contact!!! 















